#ifndef FOLDER_H
#define FOLDER_H

#include <stdlib.h>

struct Folder {
  struct Folder* parent;
  struct Folder* child;
  struct Folder* sibling;
  char* name;
};

struct Folder* getRootFolder(struct Folder* folder){
  struct Folder* root = folder;
  while(folder->parent != NULL){
    root = folder->parent;
  }
  return root;
}

struct Folder* addChild(struct Folder* parent){
  struct Folder* new_folder = malloc(sizeof(struct Folder)); //TODO, check if malloc succeeded, also add destroyFolderTree() function
  new_folder->parent = parent;
  if(parent->child == NULL){
    parent->child = new_folder;
    return new_folder;
  }
  struct Folder* sibling = parent->child;
  while(sibling->sibling != NULL){
    sibling = sibling->sibling;
  }
  sibling->sibling = new_folder;
  return new_folder;
}

void setFolderName(struct Folder* folder, char* name){
  folder->name = name;
}

void printFolderName(struct Folder* folder){
  printf("%s\n", folder->name);
}

#endif
