#include <stdio.h>

#include "folder.h"

int main(){
  struct Folder root;
  setFolderName(&root, "le root, c\'est moi.");
  printFolderName(&root);
  struct Folder* baby = addChild(&root);
  setFolderName(baby, "wah wah!");
  printFolderName(root.child);
  free(baby);

  return 0;
}

/*
  == IDEAS ==
  Scan the entire folder layout once at startup and store it as a tree of sorts,
  then check for errors, and maybe parse the tree into more succinct data before using it.
 */
